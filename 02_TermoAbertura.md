---

Autor: Thiago Serra Ferreira de Carvalho
E-mail: thiago.carvalho (at) univag.edu.br
Data: 03.08.2023
TOCTitle: TermoAbertura

---

# Termo de abertura
<!--
    Autorizar o início do projeto, atribuir principais responsáveis e documentar 
    requisitos iniciais, principais entregas, premissas e restrições.
-->

## Justificativa do projeto
<!--
    Em poucas palavras, um ou dois parágrafos, dizer o que levou a empresa a necessidade de implementar a solução.
-->

Com o avanço da tecnologia, e necessidade de automação, a empresa xyz s.a., decidiu por contratar a consultoria abc s.a., para auxiliar na confecção do projeto de implementação da solução xxx.
Dessa forma o projeto visa entrega de um produto de software que irá auxiliar em:
- Descreva aqui pelo menos 03 melhorias previstas


## Requisitos 
<!--
    O objetivo final das entrevistas é ouvir as partes interessadas e registrar suas necessidades.
    Os requisitos são registrados no seu maior nível de detalhe, pois são usados para definir os entregáveis ou marcos do projeto.
-->

- R1: Ser capaz de incluir os dados de produto contendo identificador único, nome, valor e quantidade em estoque.
- R2: Possibilitar o controle de quantidade em estoque, emitindo alerta quando esta atingir o valor mínimo cadastrado.
- R3: ...


## Premissas
<!-- 
    Aqui é o lugar para descrever o que é dado como certo para execução deste projeto.
    Por exemplo: se vamos desenvolver um software, pré supõe-se que a empresa tenha a infra estrutra para rodá-lo ou,
que possua o servidor de aplicação com requisito necessário para tal ou, ainda, que tenha pessoal técnico para ser treinado
a fim de dar suporte.
    Este é o local para fazer isso!
-->
A implementação da solução pressupõe que:
- A organização irá disponibilizar pessoal técnico que será treinado nesta, a fim de dar suporte aos usuários da organização;
- Que há na organização equipamentos com no mínimo a seguinte configuração ...


## Restrições
<!--
    O que limita o funcionamento da solução que será desenvolvida ou implantada?
    O parque computacional da empresa?
    A falta de um processo definido?
    O tempo para implantação?
-->
Este projeto pode ser limitado devido a:
- Inadequação do parque tecnológico existente na empresa tendo em vista as máquinas apresentarem ....


# Aprovações
<!--
    Declaração de aceite é importantíssimo!
    Fechado o escopo e requisitos, o projeto terá um cronograma definido.
    Alterações no escopo ou requisitos, alteram o projeto, custo, tempo ...
    É sua única forma de defesa!
-->
Este artefato foi aprovado em reunião realizada em ....
Estavam presentes o Patrocinador(a), sr(a)... e o gerente de projetos ...
Que assinam nesta data.



---
Ir para:
- [<< 01 - Plano do Projeto](01_PlanoProjeto.md)
- [03 - Cronograma >>](03_Cronograma.md)
